const { mix } = require('laravel-mix');
let _env = require('./env.laravel.mix.js');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
 |--------------------------------------------------------------------------
 | Configuration
 |--------------------------------------------------------------------------
 */
mix.setPublicPath(_env.distributionPath);

/*
 |--------------------------------------------------------------------------
 | BrowserSync
 |--------------------------------------------------------------------------
 */
if (process.env.NODE_ENV === "development") {
  let browserSync = _env.browserSync;

  mix.browserSync({
    host: 'localhost',
    proxy: browserSync.proxy,
    files: browserSync.files,
    open: browserSync.open,
    reload: false,
    logFileChanges: false,
    injectChanges: true,
    reloadOnRestart: true
  });
}

/*
 |--------------------------------------------------------------------------
 | SASS
 |--------------------------------------------------------------------------
 */
let sass = _env.sass;
let sassOptions = {
  processCssUrls: false
};

// Compile: vendor
if (sass.vendor.entryPoint && sass.vendor.outputFileName) {
  mix.sass(sass.vendor.entryPoint, sass.vendor.outputFileName).options(sassOptions);
}

// Compile: site
if (sass.site.entryPoint && sass.site.outputFileName) {
  mix.sass(sass.site.entryPoint, sass.site.outputFileName).options(sassOptions);
}

/*
 |--------------------------------------------------------------------------
 | Javascript
 |--------------------------------------------------------------------------
 */
let scripts = _env.scripts;

// Compile: vendors
let vendorScripts = scripts.vendor;
if (vendorScripts.files && vendorScripts.files.length) {
  mix.scripts(vendorScripts.files, vendorScripts.outputFileName);
}

// Compile: site
let assetsScripts = scripts.assets;
if (assetsScripts.files && assetsScripts.files.length) {
  mix.scripts(assetsScripts.files, assetsScripts.outputFileName);
}

/*
 |--------------------------------------------------------------------------
 | Full API
 |--------------------------------------------------------------------------
 | Source: https://github.com/laravelista/laravel-mix-without-laravel
 |
 */
// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.standaloneSass('src', output); <-- Faster, but isolated from Webpack.
// mix.fastSass('src', output); <-- Alias for mix.standaloneSass().
// mix.less(src, output);
// mix.stylus(src, output);
// mix.postCss(src, output, [require('postcss-some-plugin')()]);
// mix.browserSync('my-site.dev');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.sourceMaps(); // Enable sourcemaps
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.options({
//   extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
//   processCssUrls: true, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
//   purifyCss: false, // Remove unused CSS selectors.
//   uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
//   postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });