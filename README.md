# laravel-mix-boilerplate

Laravel Mix assets compilation (SASS + JavaScript) + hot reloading CSS without Laravel.

### Prerequisites

- webpack.mix.js
- example.env.laravel.mix.js
- package.json

```
 ...
 
 "dependencies": {
    "cross-env": "5.1.3", 
    "laravel-mix": "1.7.2",
    "browser-sync": "2.23.6",
    "browser-sync-webpack-plugin": "1.2.0"
  }
  
 ...
  
 "scripts": {
    "watch": "node node_modules/cross-env/dist/bin/cross-env.js NODE_ENV=development node_modules/webpack/bin/webpack.js --watch --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js",
    "production": "node node_modules/cross-env/dist/bin/cross-env.js NODE_ENV=production node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js",
    "build": "npm run production"
 }
 ...
```

### Steps

```
Step 1: Copy and rename the file "example.env.laravel.mix.js" => "env.laravel.mix.js".
Step 2: Add a new line in your .gitignore file => "/env.laravel.mix.js".
Step 3: Set your local environment configuration.
Step 4: Run "npm install" to get all dependencies.
Step 5: wait for dependencies to install...
Step 6: Start your local environment (MAMP, Vagrant, Homestead or whatever you use...).
Step 7: Run "npm run watch" to start.
Step 8: The file "mix-manifest.json" can be ignored
```

See the project repository for example and updates:
Git: https://bitbucket.org/eacontribution/laravel-mix-boilerplate/commits/all

### Commands

Start development
```
npm run watch
```

Before send to production
```
npm run build
```